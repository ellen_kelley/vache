import './Footer.scss';
import location from '../../assets/location.svg';
import tel from '../../assets/tel.svg';
import mail from '../../assets/mail.svg';
import instagram from '../../assets/instagram.svg';
import facebook from '../../assets/facebook.svg';
import logo from '../../assets/Mask Group.svg';

const Footer = () => {
    return (
        <footer className='footer'>
          <nav>
              <ul className='footer__links'>
                  <li className='footer__links--item'><img src={location} alt="location"/> <span className='footer__links--text'>Derenik Demirchyan 1/60</span></li>
                  <li className='footer__links--item'><img src={tel} alt="telephone"/> <span className='footer__links--text'>060 440944</span></li>
                  <li className='footer__links--item'><img src={mail} alt="mail"/> <span className='footer__links--text'>info@podoclinic.am</span> </li>
              </ul>
          </nav>

          <div className='footer__social-media'>
              <div className='footer__social-media--item footer__social-media--first'><img src={instagram} alt="instagram"/></div>
              <div className='footer__social-media--item'><img src={facebook} alt="facebook"/></div>
          </div>

          <div className='footer__logo'>
              <img src={logo} alt="footer-logo"/>
          </div>
        </footer>
    )
}

export default Footer;
