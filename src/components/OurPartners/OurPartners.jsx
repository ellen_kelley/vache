import './OurPartners.scss';
import img1 from '../../assets/unnamed.jpg'
import img2 from '../../assets/logo2x.jpg'

const OurPartners = () => {
    return (
        <section className='partners'>
          <div className='partners__top'>
             <div className='title'>Our Partners</div>
             <div className='line'> </div>
          </div>

          <div className='partners__content'>
             <div className='partners__img'>
                 <div>
                     <img src={img1} alt="logoPartner"/>
                 </div>
                 <div className='partners__img--bottom'>
                     <img src={img2} alt="logoPartner"/>
                 </div>
             </div>
          </div>
        </section>
    )
}

export default OurPartners;
