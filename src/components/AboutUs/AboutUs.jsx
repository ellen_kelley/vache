import './AboutUs.scss';
import {NavLink} from "react-router-dom";

const AboutUs = () => {
    return(
       <section className='about'>
         <div className='about__top'>
             <div className='about__title title'>About Us </div>
             <div className='about__line line'> </div>
         </div>
         <div className='about__text'>
             Podo Clinic is a specialized medical center dedicated to the treatment of diseases of the skin of the feet, hands and nails.
             The center is equipped with the latest German podiatric equipment, high-quality cosmetics for hands and feet
             from the famous SUDA brand, which is a reliable guarantee of quality services....
         </div>
         <NavLink to=''><div className='about__read-more'>read more</div></NavLink>

       </section>
    )
}

export default AboutUs;
