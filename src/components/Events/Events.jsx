import './Events.scss';
import slideImg from '../../assets/event_img.jpg';
import leftArrow from '../../assets/leftArrow.png';
import rightArrow from '../../assets/rightArrow.png';
import {NavLink} from "react-router-dom";

const Events = () => {
    return (
       <section className='events'>
         <div className='events__top'>
             <div className='events__title title'>Events</div>
             <div className='events__line line'> </div>
             <NavLink to='/'><span className='events__see-more'>See more</span></NavLink>
         </div>
         <div className='events__content'>
             <div>
                 <img src={slideImg} alt="slider"/>
             </div>
             <div className='events__arrows'>
                 <img src={leftArrow} alt="left-arrow" className='events__arrows--left'/>
                 <img src={rightArrow} alt="right-arrow" className='events__arrows--right'/>
             </div>
         </div>
       </section>
    )
}

export default Events;
