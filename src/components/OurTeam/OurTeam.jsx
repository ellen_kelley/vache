import './OurTeam.scss';
import teamImg from '../../assets/team-img.jpg';

const OurTeam = () => {
    return(
        <section className='team'>
           <div className='team__top'>
               <div className='title'>OurTeam</div>
               <div className='line'> </div>
           </div>

           <div className="team__content">
               <div className='team__img'>
                   <img src={teamImg} alt="slide-img"/>
               </div>
               <div className='team__stuff-info'>
                   <div className='team__stuff'>Syuzanna Gevorgyan</div>
                   <div className='team__stuff'>Doctor - Dermatologist</div>
               </div>
           </div>
        </section>
    )
}

export default OurTeam;
