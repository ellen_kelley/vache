import './Header.scss';
import logo from '../../assets/Mask Group.svg'
import menuLines from  '../../assets/menu_lines.svg';
import {NavLink} from "react-router-dom";

const Header = () => {
    return (
        <header className='header'>
           <NavLink to='/' className='header__logo'>
               <img src={logo} alt="logo_podo"/>
           </NavLink>
           <div className='header__menu'>
               <img src={menuLines} alt="menu"/>
           </div>
        </header>
    )
}

export default Header;
