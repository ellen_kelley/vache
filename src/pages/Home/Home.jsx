import './Home.scss';
import AboutUs from "../../components/AboutUs";
import Events from "../../components/Events";
import OurTeam from "../../components/OurTeam";
import OurPartners from "../../components/OurPartners";
import Footer from "../../components/Footer";

const Home = () => {
    return (
        <div className='home'>
           <AboutUs/>
           <Events/>
           <OurTeam/>
           <OurPartners/>
           <Footer/>
        </div>
    )
}

export default Home;
