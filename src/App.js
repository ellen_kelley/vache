import './App.scss';
import Header from "./components/Header";
import { BrowserRouter as Router, Route } from "react-router-dom";
import Home from "./pages/Home";

const App = () => {
  return (
      <Router>
          <Header/>
          <div>
              <Route exact path="/">
                <Home/>
              </Route>
              <Route path="/news">
              </Route>
          </div>
      </Router>
  );
}

export default App;
